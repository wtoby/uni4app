import setting from '@/config/setting.js';

class Token{
	static get(){
		try{
			return uni.getStorageSync(setting.tokenKey);
		}catch(e){
			console.error("获取token失败");
		}
	}
	
	static set(val){
		try {
		    uni.setStorageSync(setting.tokenKey, val);
		} catch (e) {
		    console.error("设置token失败");
		}
	}
	
	static remove(){
		try {
		    uni.removeStorageSync(setting.tokenKey);
		} catch (e) {
		    // error
		}
	}
}

export default Token;