import setting from '@/config/setting.js';
import Common from '@/utils/common.js';
import Token from '@/utils/token.js';

class Http{
	static _success(resolve,reject,res){
		let code = res.statusCode.toString();
		if(code.startsWith("2")){
			resolve(res.data);
		}else{
			reject();
			// TODO 请求失败
		}
	}
	
	static _fail(reject,err){
		reject();
	}
	
	/**
	 * 请求接口日志记录
	 */
	static _reqlog(req) {
		if (process.env.NODE_ENV === 'development') {
			const requestId = new Date().getTime();
			console.log("【" + requestId + "】 地址：" + req.url)
			if (req.data) {
				console.log("【" + requestId + "】 请求参数：" + JSON.stringify(req.data))
			}
		}
		//TODO 调接口异步写入日志数据库
	}
	
	static request(options,type=1){
		if(!options || !Common.isType("Object",options)){
			throw new Error("请求参数不合法！");
		}
		
		if(!options.url){
			throw new Error("请求URL不能为空！");
		}
		
		if(options.showLoading){
			uni.showLoading({
			    title: '加载中...'
			});
		}
		
		return new Promise((resolve,reject) => {
			
			const defaultOptions = {
				method: "GET",
				dataType: 'json',
				responseType: 'text',
				showLoading: false
			};
			
			options.url = Common.getFullURL(setting.baseUrl, options.url);
			options.method = options.method || defaultOptions.method;
			options.dataType = options.dataType || defaultOptions.dataType;
			options.responseType = options.responseType || defaultOptions.responseType;
			options.data = options.data || {};
			options.header = options.header || {};
			options.showLoading = options.showLoading || defaultOptions.showLoading;
			options.success = (res) => { this._success(resolve,reject,res);};
			options.fail = (err) => {this._fail(reject,err);};
			options.complete = () => {options.showLoading && uni.hideLoading();};
			// 用户token
			options.header[setting.tokenKey] = Token.get();
			// 非GET请求设置Content-Type
			if(options.method !== "GET"){
				options.header['Content-Type'] = type===1?"application/json":"application/x-www-form-urlencoded";
			}
			this._reqlog(options);
			
			uni.request(options);
		});
	}
	
	static get(url, data) {
		const options = {};
		options.url = url;
		options.data = data;
		options.method = 'GET';
		return this.request(options);
	}
		
	static post(url, data, type=1) {
		const options = {};
		options.url = url;
		options.data = data;
		options.method = 'POST';
		return this.request(options,type);
	}
		
	static put(url, data, type=1) {
		const options = {};
		options.url = url;
		options.data = data;
		options.method = 'PUT';
		return this.request(options,type);
	}
		
	static delete(url, data, type=2) {
		const options = {}
		options.url = url
		options.data = data
		options.method = 'DELETE'
		return this.request(options,type);
	}
}

export default Http;