class Common {

	/**
	 * 类型判断
	 * @param {String} type 值的类型
	 * @param {Any} val 需要判断的值
	 * @return {Boolean} 
	 */
	static isType(type, val) {
		return Object.prototype.toString.call(val) === `[object ${type}]`;
	}

	/**
	 * 判断是否是绝对地址 (有 `://`或 `//` 就算是绝对地址)
	 * @param {String} url
	 * @return {Boolean}
	 */
	static isAbsoluteURL(url) {
		return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
	}
	
	/**
	 * 组合成绝对地址的 URL (基地址+相对地址)
	 * @param {String} baseURL 基地址
	 * @param {String} relativeURL 相对地址
	 * @return {String}
	 */
	static composeURL(baseURL, relativeURL) {
	    return relativeURL ? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '') : baseURL;
	}
	
	/**
	 * 获取完整的URL
	 * @param {String|Undefined} baseURL 基地址
	 * @param {String} requestURL 相对地址
	 * @return {String}
	 */
	static getFullURL(baseURL, requestURL) {
	    if (baseURL && !this.isAbsoluteURL(requestURL)) {
	        return this.composeURL(baseURL, requestURL);
	    }
	    return requestURL;
	}
}

export default Common;
